
#define MAX_Size 256


#include <sys/types.h>
#include <iostream>
#include <cstring>


using namespace std;

struct list
{
    char * string;
    list * next;
};

void delete_one(struct list* node)
{
    free(node->string);
    free(node);
}

void delete_list(struct list *head)
{
    list *cur;
    cur = head;
    while(cur->next != NULL)
    {
        cur = cur->next;
        delete_one(cur);
    }
}

int CreateNode(struct list ** node, char * str)
{
    *node = (struct list *)malloc(sizeof(struct list *) * 1);
    if(*node == NULL)
    {
        perror("Memory allocation failure");
        return EXIT_FAILURE;
    }
    (*node)->next = NULL;
    (*node)->string = str;
    return EXIT_SUCCESS;
}

int AddNode(char* str, struct list ** head)
{
    list *last, *tmp;
    int error = CreateNode(&tmp, str);
    if(error == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }
    if(*head == NULL)
    {
        *head = tmp;
        return EXIT_SUCCESS;
    }
    for(last = *head; last->next != NULL; last->next);
    last->next = tmp;
    return EXIT_SUCCESS;
}

int get_str(char **str)
{
    char buf[MAX_Size+1] = {0};
    char *err, *tmp;
    char *line = NULL;
    int length = 1;

    for (int i = 1; buf[length - 1] != '\n'; i++)
    {
        err = fgets(buf, MAX_Size, stdin);
        if (err == NULL)
        {
            perror("Failure fgets in get_str");
            free(line);
            return EXIT_FAILURE;
        }
        length = strlen(buf);
        tmp = (char*)realloc(line, MAX_Size*i + 1);
        if(tmp == NULL)
        {
            perror("Memory allocation failure in get_str");
            free(line);
            return EXIT_FAILURE;
        }
        line = tmp;
        strcat(line, buf);
    }
    *str = tmp;
    return EXIT_SUCCESS;
}
int end_list (struct list **head)
{
    int err;
    char *str;

    do
    {
        err = get_str(&str);
        if (err == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }
        err = AddNode(str, head);
        if(err == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }
    } while (str[0] != '.');
    return EXIT_SUCCESS;
}

void print_list(list *head)
{
    list *cur;
    for(cur = head; cur->next != NULL; cur= cur->next)
    {
        printf("%s", cur->string);
    }
}

int main(int argc, char *argv[])
{ 
   list *head = NULL;
    int err_list;
    err_list = end_list(&head);
    int err_print;

    err_print = printf("PLEASE ENTER YOUR STRINGS:\n");
    if(err_print < 0)
    {
        perror("Can't print");
        return EXIT_FAILURE;
    }
    if (err_list == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    print_list(head);
    delete_list(head);
    return EXIT_SUCCESS;

}


